//
//  BetterRestApp.swift
//  BetterRest
//
//  Created by izundu ngwu on 4/4/22.
//

import SwiftUI

@main
struct BetterRestApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
