//
//  ContentView.swift
//  BetterRest
//
//  Created by izundu ngwu on 4/4/22.
//

import SwiftUI
import CoreML

struct ContentView: View {
    static var defaultWakeTime: Date {
        var components = DateComponents()
        components.hour = 5
        components.minute = 0
        return Calendar.current.date(from: components) ?? Date.now
    }
    @State private var sleepAmount = 8.0
    @State private var coffeeAmount = 1
    @State private var wakeUp = defaultWakeTime
    @State private var alertTitle = ""
    @State private var alertMessage = ""
    private var bedtime: String {
        do {
            let config = MLModelConfiguration()
            let model = try SleepCalculator(configuration: config)
            let components = Calendar.current.dateComponents([.hour, .minute], from: wakeUp)
            let hour = (components.hour ?? 0) * 60 * 60
            let minute = (components.minute ?? 0) * 60
            let prediction = try model.prediction(wake: Double(hour + minute), estimatedSleep: sleepAmount, coffee: Double(coffeeAmount+1))
            let sleepTime = wakeUp - prediction.actualSleep
            return sleepTime.formatted(date: .omitted, time: .shortened)
        } catch {
            return "Sorry, there was a problem calculating your bedtime."
        }
    }
    @State private var showingAlert = false
    
    var body: some View {
        NavigationView{
            VStack(spacing:40){
                Form{
                   Section {
                       Stepper("\(sleepAmount.formatted())", value: $sleepAmount, in: 4...12, step: 0.25)
                   } header: {
                       Text("Desired amount of sleep")
                   }
                   Section {
                       DatePicker("Enter date", selection: $wakeUp, displayedComponents: .hourAndMinute)
                           .labelsHidden()
                   } header: {
                       Text("When do you want to wake up?")
                   }
                   Section {
                       Picker("Coffee cups", selection: $coffeeAmount) {
                           ForEach(1..<21) { number in
                               Text(number == 1 ? "1 cup" : "\(number) cups")
                           }
                       }.pickerStyle(.wheel)
                   } header: {
                       Text("Daily coffee intake")
                   }
                }
                Text("Recommended Bedtime: \(bedtime)" )
                Spacer()
                
            }.navigationTitle("BetterRest")
//                .toolbar {
//                Button("Calculate", action: calculateBedtime)
//                }
                .alert(alertTitle, isPresented: $showingAlert) {
                    Button("OK") { }
                } message: {
                    Text(alertMessage)
                }
        }
    }
    func calculateBedtime() {
        do {
            let config = MLModelConfiguration()
            let model = try SleepCalculator(configuration: config)
            let components = Calendar.current.dateComponents([.hour, .minute], from: wakeUp)
            let hour = (components.hour ?? 0) * 60 * 60
            let minute = (components.minute ?? 0) * 60
            let prediction = try model.prediction(wake: Double(hour + minute), estimatedSleep: sleepAmount, coffee: Double(coffeeAmount+1))
            let sleepTime = wakeUp - prediction.actualSleep
            alertTitle = "Your ideal bedtime is…"
            alertMessage = sleepTime.formatted(date: .omitted, time: .shortened)
        } catch {
            alertTitle = "Error"
            alertMessage = "Sorry, there was a problem calculating your bedtime."
            
        }
        showingAlert = true
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
