//
//  GuessTheFlagApp.swift
//  GuessTheFlag
//
//  Created by izundu ngwu on 4/1/22.
//

import SwiftUI

@main
struct GuessTheFlagApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
