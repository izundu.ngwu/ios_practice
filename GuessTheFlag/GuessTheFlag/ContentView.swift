//
//  ContentView.swift
//  GuessTheFlag
//
//  Created by izundu ngwu on 4/1/22.
//

import SwiftUI

struct ContentView: View {
    @State private var countries = ["Estonia", "France", "Germany", "Ireland", "Italy", "Nigeria", "Poland", "Russia", "Spain", "UK", "US"]
    @State private var correctAnswer = Int.random(in: 0...2)
    @State private var showingScore = false;
    @State private var scoreTitle = "Correct/False"
    @State private var score = 0
    @State private var animationAmount = 0.0
    @State private var buttonTapped = 0
    
    fileprivate func flagtapped(_ number: Int){
        if (number == correctAnswer){
            score = score + 1;
            scoreTitle = "Correct!"
        } else {
            scoreTitle = "Wrong! That's the flag of \(countries[number])"
        }
        showingScore = true
    }
    var body: some View {
        ZStack{
            RadialGradient(stops: [
                .init(color: Color(red: 0.1, green: 0.2, blue: 0.45), location: 0.3),
                .init(color: Color(red: 0.76, green: 0.15, blue: 0.26), location: 0.3),
            ], center: .topLeading, startRadius: 200, endRadius: 400)
                .ignoresSafeArea()
            VStack(spacing: 30){
               Spacer()
                Text("Guess the Flag")
                    .foregroundColor(.white)
                    .padding()
                    .font(.largeTitle.bold())
                //Spacer()
                VStack(spacing: 15){
                    ForEach(0..<3) { number in
                        Button{
                            flagtapped(number)
                            buttonTapped = number
                        } label: {
                            Image(countries[number]).flagImage()
                        }.rotation3DEffect(.degrees(animationAmount), axis: (x: 0, y: 1, z: 0))
                    }
                    Spacer()
                    VStack(spacing: 30){
                        Text("Tap the flag of")
                            .foregroundStyle(.secondary)
                            .font(.subheadline.weight(.heavy))
                        Text("\(countries[correctAnswer])")
                            .foregroundColor(.white)
                            .font(.largeTitle.weight(.semibold))
                    }
                    
                }.frame(maxWidth: .infinity)
                    .padding(.vertical, 20)
                    .background(.ultraThinMaterial)
                    .clipShape(RoundedRectangle(cornerRadius: 15))
                Spacer()
                Spacer()
                Text("Score: \(score)")
                    .foregroundColor(.white)
                    .font(.title.bold())
                Spacer()
            }.padding()
        }.alert(scoreTitle, isPresented: $showingScore) {
            Button("Continue") {
                askQuestion()
            }
        } message: {
            Text("Your score is \(score)")
        }
    }
    func askQuestion(){
        countries.shuffle()
        correctAnswer = Int.random(in: 0..<2)
    }
}

struct FlagImage: ViewModifier {
    func body(content: Content) -> some View {
       content
//            .renderingMode(.original)
            .clipShape(RoundedRectangle(cornerRadius: 12.0))
            .shadow(radius: 5)
    }
}
extension Image {
    func flagImage() -> some View {
        modifier(FlagImage())
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
