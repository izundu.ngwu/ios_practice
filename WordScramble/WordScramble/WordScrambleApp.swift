//
//  WordScrambleApp.swift
//  WordScramble
//
//  Created by izundu ngwu on 4/9/22.
//

import SwiftUI

@main
struct WordScrambleApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
