//
//  RockPaperScissorsApp.swift
//  RockPaperScissors
//
//  Created by izundu ngwu on 4/5/22.
//

import SwiftUI

@main
struct RockPaperScissorsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
