//
//  ContentView.swift
//  RockPaperScissors
//
//  Created by izundu ngwu on 4/5/22.
//

import SwiftUI

struct ContentView: View {
    var gameplays = ["Rock", "Paper", "Scissors"]
    var winningplays = ["Paper", "Scissors", "Rock"]
    var systemImages = ["newspaper", "scissors", "shippingbox.circle.fill"]
    @State var winlose = Bool.random()
    @State var play = Int.random(in: 0..<3)
    @State var score = 0
    @State var showingAlert = false
    @State var alertTitle = "Wrong!"
    @State var alertMessage = "Paper doesn't beat Scissors"
    var body: some View {
        NavigationView{
            ZStack{
                Color(.sRGB, red: 0.118, green: 0.565, blue: 1, opacity: 1)
                    .ignoresSafeArea()
                VStack(spacing: 30){
                    Spacer()
                    Text("Opponent played \(gameplays[play].uppercased())")
                    Text("What do you play to \(winlose ? "WIN" : "LOSE")?")
                    HStack(spacing: 15){
                        ForEach(0..<3){ number in
                            Button {
                                playRound(number)
                            } label: {
                                Label("\(winningplays[number])", systemImage: systemImages[number])
                            }
                            .padding()
                            .foregroundStyle(.secondary)
                            .background(.ultraThinMaterial)
                            .clipShape(RoundedRectangle(cornerRadius: 12))
                        }
                    }
                    Spacer()
                    Text("Score: \(score)")
                        .padding()
                        .font(.title)
                    Spacer()
                }
            }
            .navigationTitle(Text("RPS RELOADED"))
            .navigationBarTitleDisplayMode(.automatic)
        }.alert("\(alertTitle)", isPresented: $showingAlert) {
            Button("dismiss") {
                showingAlert = false
            }
        } message: {
            Text(alertMessage)
        }
    }
    func playRound(_ number: Int){
       if (winningplays[number] == gameplays[play]){
            alertTitle = "Wrong!"
            alertMessage = "\(winningplays[number]) and \(gameplays[play]) are the same thing"
       }
        else if (winlose == true){
            if (number == play) {
                score += 1
                alertTitle = "Correct!"
                alertMessage = "\(winningplays[number]) beats \(gameplays[play])"
            }
            if (number == (play + 1) % 3) {
                alertTitle = "Wrong!"
                alertMessage = "\(winningplays[number]) doesn't beat \(gameplays[play])"
            }
        }
        else if (winlose == false) {
            if (number == (play + 1) % 3) {
                score += 1
                alertTitle = "Correct!"
                alertMessage = "\(winningplays[number]) loses to \(gameplays[play])"
            }
            if (number == play) {
                alertTitle = "Wrong!"
                alertMessage = "\(winningplays[number]) doesn't lose to \(gameplays[play])"
            }
        }
        else {
            alertTitle = "right and wrong, what is life?!"
            alertMessage = "beautiful tunnels..."
        }
        play = Int.random(in: 0..<3)
        winlose = Bool.random()
        showingAlert = true
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
