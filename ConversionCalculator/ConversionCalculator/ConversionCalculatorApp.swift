//
//  ConversionCalculatorApp.swift
//  ConversionCalculator
//
//  Created by izundu ngwu on 4/5/22.
//

import SwiftUI

@main
struct ConversionCalculatorApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
