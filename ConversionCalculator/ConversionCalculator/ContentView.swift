//
//  ContentView.swift
//  ConversionCalculator
//
//  Created by izundu ngwu on 4/5/22.
//

import SwiftUI

struct ContentView: View {
    @State private var units = ["meters", "kilometers", "yards", "feet", "miles"]
    @State private var input = "meters"
    @State private var output = "kilometers"
    @State private var value = "0.0"
    private var decValue : Double {
        let valueD = Double(value) ?? 0.0
        return calculateUnits(input, output, valueD) ?? 0.0
    }
    @FocusState private var focusedState : Bool
    var body: some View {
        NavigationView{
            Form{
                Section{
                    Picker("", selection: $input) {
                        ForEach(units, id: \.self) { unit in
                            Text("\(unit)")
                        }
                    }.pickerStyle(.menu)
                } header: {
                    Text("Pick the unit to convert from")
                }
                Section{
                    Picker("", selection: $output) {
                        ForEach(units, id: \.self) {
                            Text("\($0)")
                        }
                    }.pickerStyle(.menu)
                } header: {
                    Text("Pick the unit to convert to")
                }
                Section {
                    TextField("title", text: $value, prompt: Text("Enter the number in \(input)"))
                        .keyboardType(.decimalPad)
                        .focused($focusedState)
                        .toolbar {
                            ToolbarItemGroup(placement: .keyboard){
                                Button("Done") {
                                    focusedState = false
                                }
                            }
                        }
                } header: {
                    Text("Calculate")
                }
                Section {
                    Text ("\(value) \(input) = \(decValue.formatted()) \(output)")
                } header: {
                    Text("Result")
                }
            }.navigationTitle("Conversion Calculator")
        }
    }
    fileprivate func calculateUnits(_ input: String, _ output: String, _ value: Double) -> Double? {
        if (input == output) {
            return value
        }
        switch input {
        case "meters":
            switch output {
            case "kilometers":
                return value * 0.001
            case "feet":
                return value * 3.28
            case "yards":
                return value * 1.09
            case "miles":
                return value * 0.00062
            default:
                return nil
            }
        case "kilometers":
            switch output {
            case "meters":
                return value * 1000
            case "feet":
                return value * 3280.84
            case "yards":
                return value * 1093.61
            case "miles":
                return value * 0.62
            default:
                return nil
            }
        case "feet":
            switch output {
            case "kilometers":
                return value * 0.0003
            case "meters":
                return value * 0.3
            case "yards":
                return value * 0.33
            case "miles":
                return value * 0.00019
            default:
                return nil
            }
        case "yards":
            switch output {
            case "kilometers":
                return value * 0.00091
            case "feet":
                return value * 3
            case "meters":
                return value * 0.91
            case "miles":
                return value * 0.00057
            default:
                return nil
            }
        case "miles":
            switch output {
            case "kilometers":
                return value * 1.61
            case "feet":
                return value * 5280
            case "yards":
                return value * 1760
            case "meters":
                return value * 1609.34
            default:
                return nil
            }
        default:
            return nil
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
