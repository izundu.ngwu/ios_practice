//
//  ViewModifier.swift
//  ViewsAndModifiers
//
//  Created by izundu ngwu on 4/4/22.
//

import SwiftUI

struct CapsuleText : View {
    var text: String
    var body: some View {
        Text(text)
            .foregroundColor(.red)
            .padding()
            .font(.largeTitle)
            .overlay(RoundedRectangle(cornerRadius: 12).stroke( lineWidth: 2).foregroundColor(.red))
            .background(.ultraThinMaterial)
    }
}
struct ShadowTitle: ViewModifier {
    func body(content: Content) -> some View {
        content.foregroundColor(.blue)
            .padding()
            .font(.largeTitle)
            .background(.ultraThinMaterial)
            .clipShape(Capsule())
            .shadow(radius: 5)
    }
}
struct Watermark: ViewModifier {
    var text: String
    func body(content: Content) -> some View {
        ZStack(alignment: .bottomTrailing){
            VStack {
                content
                Spacer()
            }.frame(maxWidth: .infinity)
                .padding()
            Text(text)
                .foregroundColor(.blue)
//                .padding()
                .font(.footnote)
                .background(.ultraThinMaterial)
                .clipShape(Capsule())
                .shadow(radius: 5)
        }
    }
}
struct LargeBlueTitle: ViewModifier {
    func body(content: Content) -> some View {
        content
            .font(.largeTitle)
            .foregroundColor(.blue)
            .padding()
            .border(.ultraThinMaterial, width: 2)
            .clipShape(RoundedRectangle(cornerRadius: 12))
    }
}
extension View {
    func titlestyle() -> some View {
        modifier(ShadowTitle())
    }
    func watermarked(with text: String) -> some View {
        modifier(Watermark(text: text))
    }
    func makeBlueTitle() -> some View {
        modifier(LargeBlueTitle())
    }
}
