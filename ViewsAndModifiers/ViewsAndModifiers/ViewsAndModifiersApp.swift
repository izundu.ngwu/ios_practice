//
//  ViewsAndModifiersApp.swift
//  ViewsAndModifiers
//
//  Created by izundu ngwu on 4/4/22.
//

import SwiftUI

@main
struct ViewsAndModifiersApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
