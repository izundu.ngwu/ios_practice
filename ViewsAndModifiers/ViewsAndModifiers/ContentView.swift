//
//  ContentView.swift
//  ViewsAndModifiers
//
//  Created by izundu ngwu on 4/4/22.
//

import SwiftUI

struct GridStack<Content: View>: View {
    let rows: Int
    let columns: Int
    let content: (Int, Int) -> Content

    var body: some View {
        VStack {
            ForEach(0..<rows, id: \.self) { row in
                HStack {
                    ForEach(0..<columns, id: \.self) { column in
                        content(row, column)
                    }
                }
            }
        }
    }
}
struct ContentView: View {
    var body: some View {
        VStack(spacing: 30){
            CapsuleText(text: "first").shadow(radius: 2)
            Text("second").titlestyle()
            Text("Third").makeBlueTitle()
            GridStack(rows: 4, columns: 4) { row, col in
                HStack {
                        Image(systemName: "\(row * 4 + col).circle")
                        Text("R\(row) C\(col)")
                    }
            }
        }
        .watermarked(with: "zizouworks")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
