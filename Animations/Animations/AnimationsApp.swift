//
//  AnimationsApp.swift
//  Animations
//
//  Created by izundu ngwu on 4/9/22.
//

import SwiftUI

@main
struct AnimationsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
